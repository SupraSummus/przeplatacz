%% Jan Rydzewski
%% indeks 332461

:- ensure_loaded(library(lists)).
:- op(700, xfx, <>).

%% verify(+N, +PlikProgramu)

verify(N, PlikProgramu) :-
	N >= 1,
	wczytaj(PlikProgramu, Program),
%	programDaSięWykonaćNiebezpiecznie(Program, N),
%	write('Program nie jest bezpieczny.'), nl.
	(programDaSięWykonaćNiebezpiecznie(Program, N) ->
		write('Program nie jest bezpieczny.'), nl ;
		write('Program jest bezpieczny.'), nl
	).

verify(N, _) :-
	N =< 0,
	write('Liczba procesów musi być >= 1.'), nl,
	fail.

%% initState(+Program, +N, -StanPoczątkowy)
%% StanPoczątkowy jest stanem początkowym dla danego programu Program i liczby
%% procesów N.
%%
%% Reprezentacja stanu:
%%   stan(LicznikiInstrukcji, Zmienne, Tablice) gdzie
%%     * LicznikiIntrukcji to lista liczników pozycji. Indeks w liście odpowiada
%%       id procesu.
%%     * Zmienne to lista wartości zmiennych. Indeks w liście odpowiada zmiennej
%%       o tym samym indeksie w liście nazw zmiennych (tej z opisu programu).
%%     * Tablice to lista wartości tablic. Indeks w liście odpowiada tablicy o
%%       tym samym indeksie w liście nazw tablic. Każda wartość tablicy
%%       reprezentowana jest przez listę w oczywisty sposób.
initState(program(NazwyZmiennych, NazwyTablic, _), N, stan(LicznikiInstrukcji, Zmienne, Tablice)) :-
	powiel(1, N, LicznikiInstrukcji),
	length(NazwyZmiennych, LiczbaZmiennych), powiel(0, LiczbaZmiennych, Zmienne),
	powiel(0, N, WzórTablicy),
	length(NazwyTablic, LiczbaTablic), powiel(WzórTablicy, LiczbaTablic, Tablice).

%% step(+Program, +StanWe, ?PrId, -StanWy)
%% StanWy jest stanem po wykonaniu instrukcji programu Program przez proces o
%% numerze PrId w stanie StanWe.
step(program(NZ, NT, LI), stan(I, Z, T), N, stan(Ip, Zp, Tp)) :-
	nth0(N, I, NumerInstrukcji),
	nth1(NumerInstrukcji, LI, Instrukcja),
	wykonaj(Instrukcja, NZ, NT, Z, T, N, NumerInstrukcji, Zp, Tp, NumerInstrukcjiPo),
	podmień(I, N, NumerInstrukcjiPo, Ip).

wczytaj(PlikProgramu, program(Zmienne, Tablice, Instrukcje)) :-
	set_prolog_flag(fileerrors, off),
	seeing(Old),
	(see(PlikProgramu) ->
		read(vars(Zmienne)),
		read(arrays(Tablice)),
		read(program(Instrukcje)),
		seen,
		see(Old)
	;
		write('Nie udało się otworzyć pliku: '), write(PlikProgramu), nl,
		fail
	).

%% programDaSięWykonaćNiebezpiecznie(+Program, +N).
programDaSięWykonaćNiebezpiecznie(program(Z, T, I), N) :-
	Program = program(Z, T, I),
	initState(Program, N, StanPoczątkowy),

	wykonajTrochęBFSem(
		Program, N,
		[StanPoczątkowy | K] : K, [[] | KS] : KS,
		[], [Stan | Ścieżka], NumerStanu
	),
	stan(LicznikiInstrukcji, _, _) = Stan,
	naruszenieBezpieczeństwa(I, LicznikiInstrukcji, 1),

	write('Naruszenie bezpieczeństwa w stanie: '), write(NumerStanu), nl,
	reverse([Stan | Ścieżka], RŚcieżka),
	wypiszPrzeplot(RŚcieżka).

%% wykonajTrochęBFSem(Program, N,
%%   StanyDoOdwiedzenia, Ścieżki, Odwiedzone, Ścieżka, NumerStanu)
%% Ścieżka jest listą powstałą przez konkatencję pewnej listy (S) z listy
%% różnicowej Ścieżki oraz listy stanów wykonania zera lub więcej kroków
%% programu Program przez N procesów, poczynając od stanu z listy różnicowej
%% StanyDoOdwiedzenia (z pozycji odpowiadającej pozycji S w liście Ścieżki),
%% oraz bez osiągania jakiegokolwiek stanu z listy Odwiedzone.
%% NumerStanu to liczba stanów w liście Odwiedzone w momencie osiągnięcia
%% końca ścieżki.

wykonajTrochęBFSem(_, _,
	[StanPoczątkowy | _] : _, [Ścieżka | _] : _,
	Odwiedzone, [StanPoczątkowy | Ścieżka], NumerStanu
) :-
	\+ member(StanPoczątkowy, Odwiedzone),
	length([StanPoczątkowy | Odwiedzone], NumerStanu).

wykonajTrochęBFSem(Program, N,
	[Stan | Stany] : K, [Ścieżka | Ścieżki] : KS,
	Odwiedzone, ŚcieżkaKońcowa, NumerStanu
) :-
	\+ member(Stan, Odwiedzone),
	wszystkieMożliwości(Program, 0, N, Stan, K, KK, Ścieżka, KS, KKS),
	wykonajTrochęBFSem(Program, N,
		Stany : KK, Ścieżki : KKS,
		[Stan | Odwiedzone], ŚcieżkaKońcowa, NumerStanu
	).

wykonajTrochęBFSem(Program, N,
	[Stan | Stany] : K, [_ | Ścieżki] : KS,
	Odwiedzone, ŚcieżkaKońcowa, NumerStanu
) :-
	var(K), % nie wyciągaj z pustej listy
	member(Stan, Odwiedzone),
	wykonajTrochęBFSem(Program, N,
		Stany : K, Ścieżki : KS,
		Odwiedzone, ŚcieżkaKońcowa, NumerStanu
	).

%% wszystkieMożliwości(Program, Start, Stop, Stan, StanyKońcowe, K, Ścieżka, Ścieżki, KS)
%% StanyKońcowe to lista (zakończona K) stanów osiągalnych ze stanu Stan po
%% wykonaniu jeden instrukcji programu Program przez proces wybrany z zakresu
%% pidów [Start, Stop). Ścieżki to lista, zakończona KS, list stanów poprzez
%% które można osiągnąć odpowiadające stany z listy StanyKońcowe. Ścieżka
%% to ścieżka którą można osiągnąć stan Stan.
wszystkieMożliwości(_, Start, Start, _, K, K, _, KS, KS).
wszystkieMożliwości(
	Program, Start, Stop,
	Stan, [StanKońcowy | ResztaStanów], K,
	Ścieżka, [[Stan | Ścieżka] | ResztaScieżek], KS
) :-
	Start < Stop,
	step(Program, Stan, Start, StanKońcowy),
	Start1 is Start + 1,
	wszystkieMożliwości(Program, Start1, Stop,
		Stan, ResztaStanów, K,
		Ścieżka, ResztaScieżek, KS
	).

%% naruszenieBezpieczeństwa(Instrukcje, Liczniki, I).
%% Stan wykonania reprezentowany przez listę liczników instrukcji Liczniki
%% narusza bezpieczeństwo sekcji w programie reprezentowanym przez listę
%% instrukcji Instrukcje, indeksowaną od I.
naruszenieBezpieczeństwa([sekcja | _], Liczniki, I) :-
	indeksy(Liczniki, I, Ix, 0), length(Ix, N),
	N >= 2,
	write('Sekcja pod adresem: '), write(I), nl,
	write('Procesy w sekcji: '), write(Ix), nl.
naruszenieBezpieczeństwa([sekcja | Instrukcje], Liczniki, I) :-
	policz(Liczniki, I, N),
	N =< 1,
	I1 is I + 1,
	naruszenieBezpieczeństwa(Instrukcje, Liczniki, I1). 
naruszenieBezpieczeństwa([Instrukcja | Instrukcje], Liczniki, I) :-
	Instrukcja \= sekcja,
	I1 is I + 1,
	naruszenieBezpieczeństwa(Instrukcje, Liczniki, I1).

wypiszPrzeplot([_]).
wypiszPrzeplot([stan(I, _, _), stan(II, Z, T) | R]) :-
	select(X, I, Y, II),
	nth0(Ix, I, X), nth0(Ix, II, Y),
	write('Proces '), write(Ix), write(': '), write(X), nl,
	wypiszPrzeplot([stan(II, Z, T) | R]).

%% wykonaj(+I, +NZ, +NT, +Z, +T, +Pid, +NI, -Zp, -Tp, -NIp)
%% Zp, Tp i NIp to stan zmiennych, tablic i wskaźnika instrukcji po wykonaniu
%% instrukcji I (o numerze NI) przez proces o numerze Pid w środowisku z nazwami
%% zmiennych i tablic NZ i NT, oraz wartościami zmiennych i tablic Z i T.
wykonaj(assign(arr(N, WI), WW), NZ, NT, Z, T, Pid, NI, Z, Tp, NIp) :-
	NIp is NI + 1,
	oblicz(WI, NZ, NT, Z, T, Pid, I),
	oblicz(WW, NZ, NT, Z, T, Pid, W),
	nth0(NumerTablicy, NT, N),
	nth0(NumerTablicy, T, Tablica),
	podmień(Tablica, I, W, TablicaPo),
	podmień(T, NumerTablicy, TablicaPo, Tp).
wykonaj(assign(N, WW), NZ, NT, Z, T, Pid, NI, Zp, T, NIp) :-
	NIp is NI + 1,
	atom(N),
	oblicz(WW, NZ, NT, Z, T, Pid, W),
	nth0(NumerZmiennej, NZ, N),
	podmień(Z, NumerZmiennej, W, Zp).
wykonaj(goto(NIp), _, _, Z, T, _, _, Z, T, NIp).
wykonaj(condGoto(WW, _), NZ, NT, Z, T, Pid, NI, Z, T, NIp) :-
	obliczWartośćLogiczną(WW, NZ, NT, Z, T, Pid, false),
	NIp is NI + 1.
wykonaj(condGoto(WW, NIp), NZ, NT, Z, T, Pid, _, Z, T, NIp) :-
	obliczWartośćLogiczną(WW, NZ, NT, Z, T, Pid, true).
wykonaj(sekcja, _, _, Z, T, _, NI, Z, T, NIp) :- NIp is NI + 1.

%% oblicz(+Wyrażenie, +NZ, +NT, +Z, +T, +Pid, -Wartość)
%% Wartość jest wartością wyrażenia Wyrażenie w środowisku z nazwami zmiennych
%% i tablic NZ i NT, wartościami zmiennych i tablic Z i T, oraz
%% identyfikatorem wykonującego procesu Pid.
oblicz(pid, _, _, _, _, Pid, Pid).
oblicz(X, _, _, _, _, _, X) :- integer(X).
oblicz(N, NZ, _, Z, _, _, X) :-
	atom(N),
	nth0(I, NZ, N),
	nth0(I, Z, X).
oblicz(arr(N, WIndeks), NZ, NT, Z, T, Pid, X) :-
	nth0(I, NT, N),
	nth0(I, T, Tablica),

	oblicz(WIndeks, NZ, NT, Z, T, Pid, Indeks),
	length(Tablica, DługośćTablicy),
	IndeksMod is Indeks mod DługośćTablicy, % tablice są zapętlone, bo czemu nie

	nth0(IndeksMod, Tablica, X).
oblicz(W1 + W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	X is X1 + X2.
oblicz(W1 - W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	X is X1 - X2.
oblicz(W1 * W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	X is X1 * X2.
oblicz(W1 / W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	X is X1 // X2.

%% obliczWartośćLogiczną(+W, +NZ, +NT, +Z, +T, +Pid, -X)
%% analogicznie do oblicz/7
obliczWartośćLogiczną(W1 < W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	(X1 < X2 -> X = true ; X = false).
obliczWartośćLogiczną(W1 = W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	(X1 = X2 -> X = true ; X = false).
obliczWartośćLogiczną(W1 <> W2, NZ, NT, Z, T, Pid, X) :-
	oblicz(W1, NZ, NT, Z, T, Pid, X1),
	oblicz(W2, NZ, NT, Z, T, Pid, X2),
	(X1 =\= X2 -> X = true ; X = false).

%% T to tablica o długości N zawierająca tylko elementy X
powiel(X, N, T) :- length(T, N), maplist(=(X), T).

%% podmień(T, +N, X, Tp)
%% Lista Tp to lista T z elementem na pozycji N zamienionym na X.
podmień([_ | T], 0, X, [X | T]).
podmień([E | T], N, X, [E | Tp]) :- N1 is N - 1, N1 >= 0, podmień(T, N1, X, Tp).

%% policz(+T, +X, -N)
%% element X występuje w liście T dokładnie N razy
policz(T, X, N) :- indeksy(T, X, Ix, 0), length(Ix, N).

%% indeksy(+T, +X, -Ix, +I)
%% element X występuje w liście T indeksowanej od I na pozycjach Ix
indeksy([], _, [], _).
indeksy([X | T], X, [I | Ix], I) :- I1 is I + 1, indeksy(T, X, Ix, I1).
indeksy([E | T], X, Ix, I) :- E \= X, I1 is I + 1, indeksy(T, X, Ix, I1).
